//----------------------------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

use std::{
    fmt::Debug,
    fs, io, mem,
    path::{Path, PathBuf},
};

use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};
use thiserror::Error;

//----------------------------------------------------------------------------------------------------------------------
//-- TRAITS ------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

pub trait ToValue: Debug {
    fn to_value(&self) -> Value;
}

impl<T: Serialize + Debug> ToValue for T {
    fn to_value(&self) -> Value {
        serde_json::to_value(self).expect("Failed to serialize value")
    }
}

//----------------------------------------------------------------------------------------------------------------------
//-- ENUMS -------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

#[derive(Debug, Copy, Clone)]
pub enum Format {
    JSON,
    TOML,
    YAML,
}

//----------------------------------------------------------------------------------------------------------------------

#[derive(Debug, Error)]
pub enum Error {
    #[error("Serialisation failed: {0}")]
    SerialisationFailed(String),

    #[error("Deserialisation failed: {0}")]
    DeserialisationFailed(String),

    #[error("Failed to load/save file")]
    IoError(#[from] io::Error),

    #[error("Type mismatch, expected {0}, found {1}")]
    TypeMismatch(Value, Value),
}

//----------------------------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub enum Source {
    File {
        format: Format,
        path:   PathBuf,
    },
    String {
        format: Format,
        source: String,
    },
    Serialisable {
        root_key: Option<String>,
        source:   Box<dyn ToValue>,
    },
}

impl Source {
    pub fn file(format: Format, path: &Path) -> Self {
        Self::File {
            format,
            path: path.to_owned(),
        }
    }

    pub fn string(format: Format, source: impl ToString) -> Self {
        Self::String {
            format,
            source: source.to_string(),
        }
    }

    pub fn serialisable<T>(root_key: Option<impl ToString>, source: T) -> Self
    where
        T: 'static + Serialize + Debug,
    {
        Self::Serialisable {
            root_key: root_key.map(|f| f.to_string()),
            source:   Box::new(source),
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
//-- STRUCTS -----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

#[derive(Debug, Default)]
pub struct Builder {
    sources:   Vec<Source>,
    overrides: Option<Source>,
}

impl Builder {
    pub fn add_source(mut self, source: Source) -> Self {
        self.sources.push(source);
        self
    }

    pub fn add_override(mut self, format: Format, path: &Path) -> Self {
        assert!(self.overrides.is_none(), "Only one override can be set");

        self.overrides = Some(Source::File {
            format,
            path: path.to_owned(),
        });

        self
    }

    pub fn build(self) -> Result<Config, Error> {
        let mut config = Config::default();

        for s in self.sources {
            config.add_source(s)?;
        }

        if let Some(Source::File { format, ref path }) = self.overrides {
            config.load_overrides(format, path)?;
        }

        Ok(config)
    }
}

//----------------------------------------------------------------------------------------------------------------------

#[derive(Debug, Default)]
pub struct Config {
    base:      Value,
    overrides: Value,
}

impl Config {
    #[inline]
    pub fn builder() -> Builder {
        Builder::default()
    }

    pub fn add_source(&mut self, source: Source) -> Result<&mut Self, Error> {
        match source {
            Source::File { format, path } => self.add_file_new(format, &path),
            Source::String { format, source } => self.add_string_new(format, &source),
            Source::Serialisable { root_key, source } => self.add_value_new(root_key, source.to_value()),
        }?;

        Ok(self)
    }

    pub fn get<T: for<'a> Deserialize<'a>>(&self, key: &str) -> Option<T> {
        let base = find_key_value(key, &self.base);
        let overrides = find_key_value(key, &self.overrides);

        match (base, overrides) {
            (Some(mut base), Some(overrides)) => {
                merge(&mut base, overrides).ok()?;
                Some(base)
            }
            (Some(base), None) => Some(base),
            (None, Some(overrides)) => Some(overrides),
            _ => None,
        }
        .map(|v| serde_json::from_value(v).ok())?
    }

    pub fn set(&mut self, key: &str, value: impl Serialize) -> Result<(), Error> {
        let value = serde_json::to_value(value).map_err(|e| Error::SerialisationFailed(e.to_string()))?;
        let value_tree = key.split('.').rev().fold(value, |acc, key_segment| {
            let mut map = Map::new();
            map.insert(key_segment.to_owned(), acc);
            Value::Object(map)
        });

        // TODO: Add a diff function to ensure only modified values are added to the overrides.

        merge(&mut self.overrides, value_tree)?;
        Ok(())
    }

    pub fn load_overrides(&mut self, format: Format, path: &Path) -> Result<&mut Self, Error> {
        let file_string = fs::read_to_string(path)?;
        add_from_string_internal(&mut self.overrides, format, &file_string)?;
        Ok(self)
    }

    pub fn save_overrides(&self, format: Format, path: &Path) -> Result<(), Error> {
        let config_str = match format {
            Format::JSON => {
                serde_json::to_string_pretty(&self.overrides).map_err(|e| Error::SerialisationFailed(e.to_string()))
            }
            Format::TOML => {
                toml::to_string_pretty(&self.overrides).map_err(|e| Error::SerialisationFailed(e.to_string()))
            }
            Format::YAML => {
                serde_yaml::to_string(&self.overrides).map_err(|e| Error::SerialisationFailed(e.to_string()))
            }
        }?;

        fs::write(path, config_str)?;
        Ok(())
    }

    fn add_file_new(&mut self, format: Format, path: &Path) -> Result<(), Error> {
        let file_string = fs::read_to_string(path)?;
        self.add_string_new(format, &file_string)?;
        Ok(())
    }

    fn add_string_new(&mut self, format: Format, source: &str) -> Result<(), Error> {
        add_from_string_internal(&mut self.base, format, source)?;
        Ok(())
    }

    fn add_value_new(&mut self, root_key: Option<String>, value: Value) -> Result<(), Error> {
        let root = if let Some(root_key) = root_key {
            Value::Object(Map::from_iter([(root_key, value)]))
        } else {
            value
        };

        merge(&mut self.base, root)?;
        Ok(())
    }
}

//----------------------------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

fn add_from_string_internal(value: &mut Value, format: Format, source: &str) -> Result<(), Error> {
    let layer = match format {
        Format::JSON => serde_json::from_str(source).map_err(|e| Error::DeserialisationFailed(e.to_string())),
        Format::TOML => toml::from_str(source).map_err(|e| Error::DeserialisationFailed(e.to_string())),
        Format::YAML => serde_yaml::from_str(source).map_err(|e| Error::DeserialisationFailed(e.to_string())),
    }?;

    merge(value, layer)?;
    Ok(())
}

//----------------------------------------------------------------------------------------------------------------------

/// Merge two instances of `serde_json::Value`.
///
/// This function combines two JSON values into one, using the following rules:
/// * JSON objects are recursively merged together.
/// * Arrays are concatenated.
/// * Primitives and nulls are overwritten.
///
/// The merge is performed in place on the first argument.
///
/// # Arguments
///
/// * `a`: A mutable reference to a `serde_json::Value`. This value will be modified to contain the merged result.
/// * `b`: A `serde_json::Value` to merge into `a`. This value will remain unchanged.
fn merge(a: &mut Value, b: Value) -> Result<(), Error> {
    match (a, b) {
        // If both values are objects, recursively merge them
        (Value::Object(a_map), Value::Object(b_map)) => {
            for (k, v) in b_map {
                merge(a_map.entry(k).or_insert(Value::Null), v)?;
            }
        }

        // If 'a' and 'b' are arrays, extend 'a' with 'b'
        (Value::Array(a_array), Value::Array(b_array)) => {
            a_array.extend(b_array);
        }

        // For all other cases, replace 'a' with 'b' if they are of the same type or if 'a' is null
        (a_val, b_val) if a_val.is_null() || mem::discriminant(a_val) == mem::discriminant(&b_val) => {
            *a_val = b_val;
        }

        // If the types of 'a' and 'b' are different, return an error
        (a_val, b_val) => return Err(Error::TypeMismatch(a_val.clone(), b_val.clone())),
    }

    Ok(())
}

//----------------------------------------------------------------------------------------------------------------------

fn find_key_value(key: &str, value: &Value) -> Option<Value> {
    // If the key is empty, assume we want to return the while config tree
    if key.is_empty() {
        return match value {
            Value::Object(_) => Some(value.clone()),
            _ => None,
        };
    }

    key.split('.')
        .try_fold(value, |current, key_segment| match current {
            Value::Object(obj) => obj.get(key_segment),
            _ => None,
        })
        .cloned()
}

//----------------------------------------------------------------------------------------------------------------------
//-- TESTS -------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use serde::{Deserialize, Serialize};

    #[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
    struct Base {
        pub first_run: bool,
        pub display:   Display,
        pub audio:     Audio,
    }

    #[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
    struct Display {
        res:          String,
        fullscreen:   bool,
        refresh_rate: i32,
    }

    #[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
    struct Audio {
        volume:  u8,
        channel: AudioChannel,
    }

    #[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
    enum AudioChannel {
        Mono,
        Stereo,
        Surround51,
        Surround71,
    }

    //------------------------------------------------------------------------------------------------------------------

    mod test_config_builder {
        use crate::config::{Config, Error, Format, Source, tests::Display};

        #[test]
        fn build() -> Result<(), Error> {
            let foo = Config::builder()
                .add_source(Source::string(Format::JSON, r#"{ "display": { "res": "800x600" } }"#))
                .add_source(Source::serialisable(
                    Some("display"),
                    Display {
                        res:          "1280x720".to_owned(),
                        fullscreen:   false,
                        refresh_rate: 60,
                    },
                ))
                .build()?;

            dbg!(foo);
            Ok(())
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    mod test_config {
        use std::env;

        use crate::config::{
            Config, Error, Format, Source,
            tests::{Audio, AudioChannel, Base, Display},
        };

        //--------------------------------------------------------------------------------------------------------------

        #[test]
        fn from_string() -> Result<(), Error> {
            let config_str_1 = r#"{ "display": { "res": "800x600" } }"#;
            let config_str_2 = r#"{ "display": { "fullscreen": false } }"#;
            let config_str_3 = r#"{ "display": { "fullscreen": true, "refresh_rate": 60 } }"#;

            let mut config = Config::default();
            config
                .add_source(Source::string(Format::JSON, config_str_1))?
                .add_source(Source::string(Format::JSON, config_str_2))?
                .add_source(Source::string(Format::JSON, config_str_3))?;

            assert_eq!(config.get::<String>("display.res").unwrap(), "800x600".to_owned());
            assert!(config.get::<bool>("display.fullscreen").unwrap());
            assert_eq!(config.get::<i32>("display.refresh_rate").unwrap(), 60);

            Ok(())
        }

        //--------------------------------------------------------------------------------------------------------------

        #[test]
        fn from_string_mixed() -> Result<(), Error> {
            let config_str_1 = r#"{ "display": { "res": "800x600" } }"#;
            let config_str_2 = r#"
                display:
                    fullscreen: false
            "#;

            let config_str_3 = r#"
                [display]
                fullscreen = true
                refresh_rate = 60
            "#;

            let mut config = Config::default();
            config
                .add_source(Source::string(Format::JSON, config_str_1))?
                .add_source(Source::string(Format::YAML, config_str_2))?
                .add_source(Source::string(Format::TOML, config_str_3))?;

            assert_eq!(config.get::<String>("display.res").unwrap(), "800x600".to_owned());
            assert!(config.get::<bool>("display.fullscreen").unwrap());
            assert_eq!(config.get::<i32>("display.refresh_rate").unwrap(), 60);

            Ok(())
        }

        //--------------------------------------------------------------------------------------------------------------

        #[test]
        fn from_serialisable() -> Result<(), Error> {
            let display = Display {
                res:          "800x600".to_owned(),
                fullscreen:   false,
                refresh_rate: 60,
            };

            let audio = Audio {
                volume:  100,
                channel: AudioChannel::Mono,
            };

            let mut config = Config::default();
            config
                .add_source(Source::serialisable(Some("display"), display.clone()))?
                .add_source(Source::serialisable(Some("audio"), audio.clone()))?;

            dbg!(&config);

            assert_eq!(config.get::<Display>("display").unwrap(), display);
            assert_eq!(config.get::<Audio>("audio").unwrap(), audio);

            Ok(())
        }

        //--------------------------------------------------------------------------------------------------------------

        #[test]
        fn get_set() -> Result<(), Error> {
            let config_str = r#"
                first_run = true

                [display]
                res = "800x600"
                fullscreen = false
                refresh_rate = 60

                [audio]
                volume = 90
                channel = "Stereo"
            "#;

            let mut config = Config::default();
            config.add_source(Source::string(Format::TOML, config_str))?;

            assert_eq!(config.get::<String>("display.res").unwrap(), "800x600".to_owned());
            assert!(!config.get::<bool>("display.fullscreen").unwrap());
            assert_eq!(config.get::<i32>("display.refresh_rate").unwrap(), 60);
            assert_eq!(
                config.get::<Audio>("audio").unwrap(),
                Audio {
                    volume:  90,
                    channel: AudioChannel::Stereo,
                }
            );

            config.set("audio.channel", AudioChannel::Surround71)?;
            config.set("display.fullscreen", true)?;
            config.set("display.refresh_rate", 144)?;

            assert_eq!(config.get::<String>("display.res").unwrap(), "800x600".to_owned());
            assert!(config.get::<bool>("display.fullscreen").unwrap());
            assert_eq!(config.get::<i32>("display.refresh_rate").unwrap(), 144);
            assert_eq!(
                config.get::<Audio>("audio").unwrap(),
                Audio {
                    volume:  90,
                    channel: AudioChannel::Surround71,
                }
            );

            // Get the whole config
            assert!(config.get::<Base>("").is_some());

            Ok(())
        }

        //--------------------------------------------------------------------------------------------------------------

        #[test]
        fn load_save_overrides() -> Result<(), Error> {
            let override_path = env::temp_dir().join("overrides.yaml");

            let config_str = r#"
                [display]
                res = "800x600"
                fullscreen = false
                refresh_rate = 60

                [audio]
                volume = 90
                channel = "Stereo"
            "#;

            {
                let mut config = Config::default();
                config.add_source(Source::string(Format::TOML, config_str))?;

                config.set("audio.volume", 50)?;
                config.set("display.res", "1280x720".to_owned())?;

                config.save_overrides(Format::YAML, &override_path)?;
            }

            let mut config = Config::default();
            config.add_source(Source::string(Format::TOML, config_str))?;

            assert_eq!(config.get::<u8>("audio.volume").unwrap(), 90);
            assert_eq!(config.get::<String>("display.res").unwrap(), "800x600");

            config.load_overrides(Format::YAML, &override_path)?;

            assert_eq!(config.get::<u8>("audio.volume").unwrap(), 50);
            assert_eq!(config.get::<String>("display.res").unwrap(), "1280x720");

            Ok(())
        }

        //--------------------------------------------------------------------------------------------------------------

        #[test]
        #[should_panic]
        fn type_mismatch() {
            let config_str1 = r#"{ "display": { "fullscreen": true } }"#;
            let config_str2 = r#"{ "display": { "fullscreen": "false" } }"#;

            Config::default()
                .add_source(Source::string(Format::JSON, config_str1))
                .unwrap()
                .add_source(Source::string(Format::JSON, config_str2))
                .unwrap();
        }
    }
}
