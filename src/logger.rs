//----------------------------------------------------------------------------------------------------------------------
// Copyright 2025 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

use std::{
    ffi::OsStr,
    fs,
    io::{ErrorKind, Write},
    path::PathBuf,
};

use parking_lot::Mutex;

//----------------------------------------------------------------------------------------------------------------------
//-- STRUCTS -----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

pub struct LoggerBuilder {
    level:         log::Level,
    log_file_path: Option<PathBuf>,
}

impl LoggerBuilder {
    pub const fn level(mut self, level: log::Level) -> Self {
        self.level = level;
        self
    }

    pub fn log_file_path<T>(mut self, path: &T) -> Self
    where
        T: ?Sized + AsRef<OsStr>,
    {
        self.log_file_path = Some(PathBuf::from(path));
        self
    }

    pub fn build(self) -> Result<(), log::SetLoggerError> {
        let log_file = self.log_file_path.map_or_else(
            || None,
            |path| {
                match fs::create_dir_all(&path) {
                    Ok(_) => {}
                    // Ignore the error if the directory already exists
                    Err(e) if e.kind() == ErrorKind::AlreadyExists => {}
                    Err(e) => panic!("{}", e),
                }

                let date = chrono::Local::now().format("%Y-%m-%d").to_string();
                let file_path = {
                    let mut path = path.clone();
                    path.push(date);
                    path.with_extension("log")
                };

                let file = fs::File::options().append(true).create(true).open(file_path).unwrap();
                Some(Mutex::new(file))
            },
        );

        let logger = Logger {
            level: self.level,
            log_file,
        };

        log::set_max_level(log::LevelFilter::max());
        log::set_boxed_logger(Box::new(logger))
    }
}

impl Default for LoggerBuilder {
    fn default() -> Self {
        Self {
            level:         log::Level::Info,
            log_file_path: None,
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

pub struct Logger {
    level:    log::Level,
    log_file: Option<Mutex<fs::File>>,
}

impl Logger {
    #[inline]
    pub fn builder() -> LoggerBuilder {
        LoggerBuilder::default()
    }

    fn format_message(&self, record: &log::Record) -> String {
        let timestamp = chrono::Local::now().format("%Y-%m-%d %H:%M:%S%.3f");
        let level = record.level();
        let target = record.target();
        let args = record.args();

        format!("{timestamp} [{level}] {target}: {args}\n")
    }
}

impl log::Log for Logger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        metadata.level() <= self.level
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            let message = self.format_message(record);
            print!("{message}");

            if let Some(log_file) = &self.log_file {
                log_file.lock().write_all(message.as_bytes()).unwrap();
            }
        }
    }

    fn flush(&self) {}
}

//----------------------------------------------------------------------------------------------------------------------
//-- TESTS -------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use log::info;

    use super::Logger;

    #[test]
    fn logger_test() {
        Logger::builder()
            .level(log::Level::Info)
            .log_file_path("./sdfdf")
            .build()
            .unwrap();

        info!(target: "foo", "test");
        info!(target: "foo", "test");
        info!(target: "foo", "test");
    }
}
