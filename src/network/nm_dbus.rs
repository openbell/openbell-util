//----------------------------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

use zbus::zvariant;

use crate::HashMap;

//----------------------------------------------------------------------------------------------------------------------
//-- TRAITS ------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

/// Interface for interacting with NetworkManager via D-BUS.
#[zbus::proxy(
    interface = "org.freedesktop.NetworkManager",
    default_service = "org.freedesktop.NetworkManager",
    default_path = "/org/freedesktop/NetworkManager"
)]
pub trait NetworkManager {
    /// Returns all devices managed my NetworkManager.
    async fn get_devices(&self) -> zbus::fdo::Result<Vec<zvariant::OwnedObjectPath>>;

    /// Activate an existing connection.
    async fn activate_connection(
        &self,
        connection: zvariant::OwnedObjectPath,
        device: zvariant::OwnedObjectPath,
        specific_object: zvariant::OwnedObjectPath,
    ) -> zbus::Result<zvariant::OwnedObjectPath>;

    /// Adds a new connection and activates it.
    async fn add_and_activate_connection(
        &self,
        connection: HashMap<&str, HashMap<&str, zvariant::OwnedValue>>,
        devices: zvariant::OwnedObjectPath,
        specific_object: zvariant::OwnedObjectPath,
    ) -> zbus::Result<(zvariant::OwnedObjectPath, zvariant::OwnedObjectPath)>;
}

//----------------------------------------------------------------------------------------------------------------------

/// Interface for interacting with the NetworkManager device API via D-BUS.
#[zbus::proxy(
    interface = "org.freedesktop.NetworkManager.Device",
    default_service = "org.freedesktop.NetworkManager"
)]
pub trait NetworkManagerDevice {
    /// Called when the device state changes.
    #[zbus(signal)]
    fn state_changed(new_state: u32, old_state: u32, reason: u32) -> zbus::Result<()>;
}

//----------------------------------------------------------------------------------------------------------------------

/// Interface for interacting with the NetworkManager wireless device API via D-BUS.
#[zbus::proxy(
    interface = "org.freedesktop.NetworkManager.Device.Wireless",
    default_service = "org.freedesktop.NetworkManager"
)]
pub trait NetworkManagerDeviceWireless {
    /// Get all access points.
    async fn get_all_access_points(&self) -> zbus::fdo::Result<Vec<zvariant::OwnedObjectPath>>;

    /// Request a scan for access points.
    async fn request_scan(&self, options: HashMap<&str, zvariant::OwnedValue>) -> zbus::fdo::Result<()>;
}

//----------------------------------------------------------------------------------------------------------------------

/// Interface for interacting with the NetworkManager access point API via D-BUS.
#[zbus::proxy(
    interface = "org.freedesktop.NetworkManager.AccessPoint",
    default_service = "org.freedesktop.NetworkManager"
)]
pub trait NetworkManagerAccessPoint {}

//----------------------------------------------------------------------------------------------------------------------

/// Interface for interacting with the NetworkManager settings API via D-BUS.
#[zbus::proxy(
    interface = "org.freedesktop.NetworkManager.Settings",
    default_service = "org.freedesktop.NetworkManager",
    default_path = "/org/freedesktop/NetworkManager/Settings"
)]
pub trait NetworkManagerSettings {
    async fn list_connections(&self) -> zbus::fdo::Result<Vec<zvariant::OwnedObjectPath>>;
    async fn add_connection_unsaved(
        &self,
        connection: HashMap<&str, HashMap<&str, zvariant::OwnedValue>>,
    ) -> zbus::Result<zvariant::OwnedObjectPath>;
}

//----------------------------------------------------------------------------------------------------------------------

#[zbus::proxy(
    interface = "org.freedesktop.NetworkManager.Settings.Connection",
    default_service = "org.freedesktop.NetworkManager"
)]
pub trait NetworkManagerSettingsConnection {
    async fn get_settings(&self) -> zbus::fdo::Result<HashMap<String, HashMap<String, zvariant::OwnedValue>>>;
    async fn save(&self) -> zbus::fdo::Result<()>;
    async fn delete(&self) -> zbus::fdo::Result<()>;
}

//----------------------------------------------------------------------------------------------------------------------

#[zbus::proxy(
    interface = "org.freedesktop.NetworkManager.IP4Config",
    default_service = "org.freedesktop.NetworkManager"
)]
pub trait NetworkManagerIP4Config {}
