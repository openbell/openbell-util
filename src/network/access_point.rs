//----------------------------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

use zbus::zvariant;

use crate::network::{NMError, connection::Connection, nm_dbus::NetworkManagerAccessPointProxy};

//----------------------------------------------------------------------------------------------------------------------
//-- STRUCTS -----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

#[derive(Debug)]
pub struct AccessPoint {
    pub ssid:     String,
    pub strength: u8,
    pub security: bool,

    /// Known connection details
    ///
    /// Is `Some` if this `AccessPoint` is a known connection (i.e. has been connected to before).
    /// Is `None` if it is not.
    pub connection: Option<Connection>,
    pub path:       zvariant::OwnedObjectPath,
}

impl AccessPoint {
    pub async fn new(
        zbus_connection: &zbus::Connection,
        path: zvariant::OwnedObjectPath,
        known_connections: &[Connection],
    ) -> Result<Self, NMError> {
        let proxy = Self::proxy(zbus_connection, path.clone()).await;

        let ssid = String::from_utf8(proxy.inner().get_property::<Vec<u8>>("Ssid").await?)
            .expect("Failed to get SSID as UTF-8");

        let strength: u8 = proxy.inner().get_property("Strength").await?;
        let security = get_security_flag(&proxy).await;

        let mut connection = None;
        for conn in known_connections {
            if let Some(s) = conn.get_ssid().await {
                if s == ssid {
                    connection = Some(conn.clone());
                    break;
                }
            }
        }

        Ok(Self {
            path: path.clone(),
            ssid,
            strength,
            security,
            connection,
        })
    }

    #[inline]
    pub const fn requires_password(&self) -> bool {
        self.security && self.connection.is_none()
    }

    /// Returns the `NetworkManagerAccessPointProxy`
    ///
    /// # Arguments
    ///
    /// * `connection`: The zbus connection
    /// * `path`: D-bus path to the access point
    ///
    /// # Panics
    ///
    /// Panics if the proxy cannot be created.
    #[inline]
    pub async fn proxy(
        connection: &zbus::Connection,
        path: zvariant::OwnedObjectPath,
    ) -> NetworkManagerAccessPointProxy {
        NetworkManagerAccessPointProxy::new(connection, path)
            .await
            .expect("Failed to create access point proxy")
    }
}

//----------------------------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

async fn get_security_flag(proxy: &NetworkManagerAccessPointProxy<'_>) -> bool {
    let wpa_flags = proxy.inner().get_property("WpaFlags").await.unwrap_or(0u32);
    let rsn_flags = proxy.inner().get_property("RsnFlags").await.unwrap_or(0u32);

    wpa_flags != 0 || rsn_flags != 0
}
