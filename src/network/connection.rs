//----------------------------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

use zbus::zvariant::{self};

use crate::{
    HashMap,
    network::{
        NMError,
        nm_dbus::{NetworkManagerSettingsConnectionProxy, NetworkManagerSettingsProxy},
    },
};

//----------------------------------------------------------------------------------------------------------------------
//-- STRUCTS -----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct Connection {
    pub path:       zvariant::OwnedObjectPath,
    pub connection: zbus::Connection,
}

impl Connection {
    pub async fn new(connection: &zbus::Connection, path: zvariant::OwnedObjectPath) -> Result<Self, NMError> {
        Ok(Self {
            path,
            connection: connection.clone(),
        })
    }

    pub async fn get_ssid(&self) -> Option<String> {
        self.get_settings()
            .await
            .ok()?
            .get("802-11-wireless")
            .and_then(|wireless_settings| wireless_settings.get("ssid"))
            .and_then(|ssid_value| ssid_value.downcast_ref::<zvariant::Array>().ok())
            .map(|array| {
                array
                    .iter()
                    .filter_map(|value| value.downcast_ref::<u8>().ok())
                    .collect::<Vec<u8>>()
            })
            .and_then(|bytes| String::from_utf8(bytes).ok())
    }

    pub async fn get_settings(&self) -> Result<HashMap<String, HashMap<String, zvariant::OwnedValue>>, NMError> {
        Ok(Self::settings_proxy(&self.connection, self.path.clone())
            .await
            .get_settings()
            .await?)
    }

    pub async fn settings_proxy(
        connection: &zbus::Connection,
        path: zvariant::OwnedObjectPath,
    ) -> NetworkManagerSettingsConnectionProxy {
        NetworkManagerSettingsConnectionProxy::new(connection, path)
            .await
            .expect("Failed to create connection settings proxy")
    }

    pub async fn list_known_connections(connection: &zbus::Connection) -> Result<Vec<Self>, NMError> {
        let known_connections = NetworkManagerSettingsProxy::new(connection)
            .await?
            .list_connections()
            .await?;

        // TODO: Replace this loop with an iterator once await in closures are stabilised.
        let mut connections = Vec::with_capacity(known_connections.len());
        for conn_path in known_connections {
            if let Ok(connection) = Self::new(connection, conn_path).await {
                connections.push(connection);
            }
        }

        Ok(connections)
    }
}
