//----------------------------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

use ahash::HashMapExt;
use futures_util::StreamExt;
use itertools::Itertools;
use tokio::sync::oneshot;
use zbus::{zvariant, zvariant::Value};

use crate::{
    HashMap,
    network::{
        NMError, NetworkManger,
        access_point::AccessPoint,
        connection::Connection,
        nm_dbus::{
            NetworkManagerDeviceProxy, NetworkManagerDeviceWirelessProxy, NetworkManagerIP4ConfigProxy,
            NetworkManagerSettingsConnectionProxy, NetworkManagerSettingsProxy,
        },
    },
};

//----------------------------------------------------------------------------------------------------------------------
//-- STRUCTS -----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

pub struct WifiDevice {
    connection: zbus::Connection,
    path:       zvariant::OwnedObjectPath,
}

impl WifiDevice {
    #[inline]
    pub fn new(connection: &zbus::Connection, path: zvariant::OwnedObjectPath) -> Self {
        Self {
            connection: connection.clone(),
            path,
        }
    }

    pub async fn request_wifi_scan(&self) -> Result<(), NMError> {
        Self::wireless_proxy(&self.connection, self.path.clone())
            .await
            .request_scan(HashMap::new())
            .await?;

        Ok(())
    }

    pub async fn list_wifi_networks(&self) -> Result<Vec<AccessPoint>, NMError> {
        let proxy = Self::wireless_proxy(&self.connection, self.path.clone()).await;
        let ap_path_list = proxy.get_all_access_points().await?;
        let mut ap_list = Vec::with_capacity(ap_path_list.len());

        let known_connections = Connection::list_known_connections(&self.connection).await?;

        // TODO: Change this loop to use `iter::map` once it is supported in async contexts.
        for ap_path in &ap_path_list {
            let ap = AccessPoint::new(&self.connection, ap_path.clone(), &known_connections).await?;
            ap_list.push(ap);
        }

        // Filter out duplicates and empty SSID's
        let ap_list = ap_list
            .into_iter()
            .filter(|e| !e.ssid.is_empty())
            .unique_by(|e| e.ssid.clone())
            .collect();

        Ok(ap_list)
    }

    pub async fn connect_to_network(
        &self,
        access_point: &AccessPoint,
        password: Option<String>,
    ) -> Result<(), NMError> {
        // Return an error if a password is required but none was supplied
        if access_point.security && (password.is_none() && access_point.connection.is_none()) {
            return Err(NMError::NoPasswordSupplied(access_point.ssid.clone()));
        }

        let ssid = &access_point.ssid;
        let proxy = NetworkManger::proxy(&self.connection).await;
        let settings_proxy = NetworkManagerSettingsProxy::new(&self.connection).await?;

        let mut new_connection = None;

        // If this is already a known connection, reconnect.
        // Otherwise, create and activate a new temporary connection.
        match &access_point.connection {
            Some(conn) => {
                proxy
                    .activate_connection(conn.path.clone(), self.path.clone(), access_point.path.clone())
                    .await?;
            }
            _ => {
                let connection_params = get_connection_params(ssid, password)?;
                let conn = settings_proxy.add_connection_unsaved(connection_params).await?;

                proxy
                    .activate_connection(conn.clone(), self.path.clone(), access_point.path.clone())
                    .await?;

                new_connection = Some(conn);
            }
        }

        // Wait for the wifi activation result.
        // If successful, save this new connection, otherwise delete it.
        let activation_result = wait_for_wifi_activation(self).await;
        if let Some(conn) = new_connection {
            let conn_proxy = NetworkManagerSettingsConnectionProxy::new(&self.connection, conn).await?;

            if activation_result.is_err() {
                conn_proxy.delete().await?;
                return activation_result;
            }

            conn_proxy.save().await?;
        }

        Ok(())
    }

    /// Returns the current IPv4 address.
    pub async fn ipv4_address(&self) -> Result<String, NMError> {
        let proxy = Self::proxy(&self.connection, self.path.clone()).await;
        let ip4_config_path: zvariant::OwnedObjectPath = proxy.inner().get_property("Ip4Config").await?;

        let ipv4_proxy = NetworkManagerIP4ConfigProxy::new(&self.connection, ip4_config_path).await?;
        let address_data: Vec<HashMap<String, zvariant::OwnedValue>> =
            ipv4_proxy.inner().get_property("AddressData").await?;

        // Just assume the first element is correct.
        // FIXME: It may not be safe to assume the first element is correct.
        //        TBD if this will be an issue.
        Ok(address_data[0]["address"].downcast_ref()?)
    }

    /// Returns the `NetworkManagerDeviceProxy`
    ///
    /// # Arguments
    ///
    /// * `connection`: The zbus connection
    /// * `path`: D-bus path to the device
    ///
    /// # Panics
    ///
    /// Panics if the proxy cannot be created.
    #[inline]
    pub async fn proxy(connection: &zbus::Connection, path: zvariant::OwnedObjectPath) -> NetworkManagerDeviceProxy {
        NetworkManagerDeviceProxy::new(connection, path)
            .await
            .expect("Failed to create device proxy")
    }

    /// Returns the `NetworkManagerDeviceWirelessProxy`
    ///
    /// # Arguments
    ///
    /// * `connection`: The zbus connection
    /// * `path`: D-bus path to the device
    ///
    /// # Panics
    ///
    /// Panics if the proxy cannot be created.
    #[inline]
    pub async fn wireless_proxy(
        connection: &zbus::Connection,
        path: zvariant::OwnedObjectPath,
    ) -> NetworkManagerDeviceWirelessProxy {
        NetworkManagerDeviceWirelessProxy::new(connection, path)
            .await
            .expect("Failed to create wireless device proxy")
    }
}

//----------------------------------------------------------------------------------------------------------------------
//-- PRIVATE FUNCTIONS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

fn get_connection_params(
    ssid: &str,
    password: Option<String>,
) -> Result<HashMap<&str, HashMap<&str, zvariant::OwnedValue>>, NMError> {
    let mut connection_params = HashMap::from_iter([
        (
            "connection",
            HashMap::from_iter([
                ("id", Value::from(ssid).try_to_owned()?),
                ("type", Value::from("802-11-wireless").try_to_owned()?),
            ]),
        ),
        (
            "802-11-wireless",
            HashMap::from_iter([
                ("ssid", Value::from(ssid.as_bytes()).try_to_owned()?),
                ("mode", Value::from("infrastructure").try_to_owned()?),
            ]),
        ),
        (
            "ipv4",
            HashMap::from_iter([("method", Value::from("auto").try_to_owned()?)]),
        ),
        (
            "ipv6",
            HashMap::from_iter([("method", Value::from("ignore").try_to_owned()?)]),
        ),
    ]);

    if let Some(password) = password {
        connection_params.insert(
            "802-11-wireless-security",
            HashMap::from_iter([
                ("key-mgmt", Value::from("wpa-psk").try_to_owned()?),
                ("psk", Value::from(password).try_to_owned()?),
            ]),
        );
    }

    Ok(connection_params)
}

async fn wait_for_wifi_activation(device: &WifiDevice) -> Result<(), NMError> {
    const DEVICE_STATE_ACTIVATED: u32 = 100;
    const DEVICE_STATE_FAILED: u32 = 120;

    let proxy = NetworkManagerDeviceProxy::builder(&device.connection)
        .path(device.path.clone())?
        .build()
        .await?;

    let (sender, receiver) = oneshot::channel();

    tokio::spawn(async move {
        let mut network_state_stream = proxy
            .receive_state_changed()
            .await
            .expect("Failed to get state change stream");

        while let Some(msg) = network_state_stream.next().await {
            let args = msg.args().expect("Failed to get state changed args");

            match args.new_state {
                DEVICE_STATE_ACTIVATED => {
                    sender.send(true).expect("Failed to send to mpsc");
                    return;
                }

                DEVICE_STATE_FAILED => {
                    sender.send(false).expect("Failed to send to mpsc");
                    return;
                }

                _ => {}
            }
        }
    });

    if receiver.await.unwrap_or(false) {
        Ok(())
    } else {
        Err(NMError::NetworkConnectionFailed)
    }
}
