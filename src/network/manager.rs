//----------------------------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

use crate::network::{NMError, access_point::AccessPoint, device::WifiDevice, nm_dbus::NetworkManagerProxy};

//----------------------------------------------------------------------------------------------------------------------
//-- STRUCTS -----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

pub struct NetworkManger {
    connection:  zbus::Connection,
    wifi_device: WifiDevice,
}

impl NetworkManger {
    pub async fn new() -> Result<Self, NMError> {
        let connection = zbus::Connection::system().await?;
        let proxy = Self::proxy(&connection).await;
        let mut wifi_device_path = None;

        // TODO: Replace this loop with an iterator once async closures stabilise.
        // Iterate through device paths to find a Wi-Fi device
        for device_path in proxy.get_devices().await? {
            let result = async {
                let device_proxy = WifiDevice::proxy(&connection, device_path.clone()).await;
                let device_type: u32 = device_proxy.inner().get_property("DeviceType").await.ok()?;
                (device_type == 2).then(|| device_path.clone())
            }
            .await;

            if result.is_some() {
                wifi_device_path = result;
                break;
            }
        }

        let wifi_device = WifiDevice::new(&connection, wifi_device_path.ok_or(NMError::NoWirelessDevice)?);
        Ok(Self {
            connection,
            wifi_device,
        })
    }

    #[inline]
    pub async fn request_wifi_scan(&self) -> Result<(), NMError> {
        self.wifi().request_wifi_scan().await
    }

    #[inline]
    pub async fn list_wifi_networks(&self) -> Result<Vec<AccessPoint>, NMError> {
        self.wifi().list_wifi_networks().await
    }

    pub async fn connect_to_network(&self, ssid: &str, password: Option<String>) -> Result<(), NMError> {
        let networks = self.list_wifi_networks().await?;

        match networks.iter().find(|ap| ap.ssid == ssid) {
            Some(ap) => self.wifi().connect_to_network(ap, password).await,
            None => Err(NMError::NetworkNotFound(ssid.to_owned())),
        }
    }

    #[inline]
    pub const fn wifi(&self) -> &WifiDevice {
        &self.wifi_device
    }

    /// Returns the `NetworkManagerProxy`
    ///
    /// # Arguments
    ///
    /// * `connection`: The zbus connection
    ///
    /// # Panics
    ///
    /// Panics if the proxy cannot be created.
    #[inline]
    pub async fn proxy(connection: &zbus::Connection) -> NetworkManagerProxy {
        NetworkManagerProxy::new(connection)
            .await
            .expect("Failed to create NetworkManager Proxy")
    }
}

//----------------------------------------------------------------------------------------------------------------------
//-- TESTS -------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use crate::network::NetworkManger;

    #[tokio::test]
    async fn network_manager_new() {
        NetworkManger::new().await.unwrap();
    }

    #[tokio::test]
    async fn request_ap_scan() {
        NetworkManger::new()
            .await
            .unwrap()
            .wifi()
            .request_wifi_scan()
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn list_wifi_networks() {
        let manager = NetworkManger::new().await.unwrap();
        let wifi = manager.wifi();
        wifi.request_wifi_scan().await.unwrap();
        dbg!(wifi.list_wifi_networks().await.unwrap());
    }
}
