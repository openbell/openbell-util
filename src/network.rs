//----------------------------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

mod access_point;
mod connection;
mod device;
mod manager;
mod nm_dbus;

pub use access_point::AccessPoint;
pub use manager::NetworkManger;

//----------------------------------------------------------------------------------------------------------------------
//-- ENUMS -------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

#[derive(Debug, thiserror::Error)]
pub enum NMError {
    #[error("")]
    ZBusError(#[from] zbus::Error),

    #[error("")]
    ZBusFdoError(#[from] zbus::fdo::Error),

    #[error("")]
    ZVariantError(#[from] zbus::zvariant::Error),

    #[error("No password supplied, but one is needed to connect to: {0}")]
    NoPasswordSupplied(String),

    #[error("Failed to connect to selected network")]
    NetworkConnectionFailed,

    #[error("No wireless devices were found")]
    NoWirelessDevice,

    #[error("Network with the SSID \"{0}\" not found")]
    NetworkNotFound(String),
}
