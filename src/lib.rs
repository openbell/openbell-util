//----------------------------------------------------------------------------------------------------------------------
// Copyright 2023 Kyle Finlay
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
//-- MODULES & IMPORTS -------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

pub mod config;
pub mod logger;
pub mod network;

use ahash::RandomState;

//----------------------------------------------------------------------------------------------------------------------
//-- TYPES -------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

#[allow(clippy::disallowed_types)]
pub type HashMap<K, V> = std::collections::HashMap<K, V, RandomState>;

#[allow(clippy::disallowed_types)]
pub type HashSet<K> = std::collections::HashSet<K, RandomState>;
